var taskManagerModule = angular.module('taskManagerApp', ['ngAnimate']);
taskManagerModule.controller('taskManagerController', function ($scope,$http) {
 var urlBase="";
 $scope.toggle=true;
 $scope.selection = [];
 $scope.statuses=['ACTIVE','DELAYED','COMPLETED'];
 $scope.priorities=['HIGH','LOW','MEDIUM'];
 $http.defaults.headers.post["Content-Type"] = "application/json";
 
 $scope.IsVisible = false;
 $scope.ShowHide = function () {
     //If DIV is visible it will be hidden and vice versa.
     $scope.IsVisible = $scope.IsVisible ? false : true;
 }
 /*
 function compare(a,b) {
	 if(a.taskTime != b.taskTime) {
		 console.log("if statement");
		 //return b.taskPriority - a.taskPriority;
		 if(a.taskPriority == "HIGH" && (b.taskPriority == "MEDIUM" || b.taskPriority == "LOW")) {
			 return 1;
		 }
		 else if(a.taskPriority == "LOW" && (b.taskPriority == "MEDIUM" || b.taskPriority == "HIGH")) {
			 return -1;
		 }
		 else {
			 if(b.taskPriority == "LOW") return 1;
			 else if(b.taskPriority == "HIGH") return -1;
		 }
		 
		 return 0;
	 }
	 else {
		 console.log("else statement");
		 return b.taskTime - a.taskTime;
	 }
 }
*/
function compare(a,b){
	if(a.taskTime == b.taskTIme) {
		return a.taskPriority - b.taskPriority;
	}
	else return 0;
}
 function findAllTasks() {
    //get all tasks and display initially
    $http.get(urlBase + '/tasks/search/findByTaskArchived?archivedFalse=0').
        success(function (data) {
            if (data._embedded != undefined) {
                $scope.tasks = data._embedded.tasks;
                //$scope.tasks.taskTIme = data._embedded.tasks.taskTime;
                $scope.tasks.sort(function(a,b) { return a.taskTime - b.taskTIme });
                $scope.tasks.sort(compare);
                console.log("data._embedded != undefined");
            } else {
                $scope.tasks = [];
                console.log("data._embedded is undefined");
            }
            
//            for (var i = 0; i < $scope.tasks.length; i++) {
//                if ($scope.tasks[i].taskStatus == 'COMPLETED') {
//                    $scope.selection.push($scope.tasks[i].taskId);
//                }
//            }            
            $scope.taskName="";
            $scope.taskDescription="";
            $scope.taskPriority="";
            $scope.taskStatus="";
            $scope.toggle='!toggle';
        });
 }
 
 findAllTasks();
 
 //add a new task
 $scope.addTask = function addTask() {
  if($scope.taskName=="" || $scope.taskDescription=="" || $scope.taskPriority == "" || $scope.taskStatus == ""){
   alert("Insufficient Data! Please provide values for task name, description, priortiy and status");
  }
  else{
	  var time = new Date();
	  var month = (parseInt(time.getMonth()) + 1).toString();
	  var datenow = time.getFullYear() + "-" + month + "-" + time.getDate();
	  console.log(time.getFullYear() + "-" + month + "-" + time.getDate());
   $http.post(urlBase + '/tasks', {
             taskName: $scope.taskName,
             taskDescription: $scope.taskDescription,
             taskPriority: $scope.taskPriority,
             taskStatus: $scope.taskStatus,
             taskTime: datenow
         }).
         success(function(data, status, headers) {
    		alert("Task added");
	         //var newTaskUri = headers()["location"];
	         //console.log("Might be good to GET " + newTaskUri + " and append the task.");
	         // Refetching EVERYTHING every time can get expensive over time
	         // Better solution would be to $http.get(headers()["location"]) and add it to the list
            findAllTasks();
      });
  }
 };
 
 // toggle selection for a given task by task id
   $scope.toggleSelection = function toggleSelection(taskUri) {
	     var idx = $scope.selection.indexOf(taskUri);
	     // is currently selected
	        // HTTP PATCH to ACTIVE state
	     if (idx > -1) {
	       $http.patch(taskUri, { taskStatus: 'ACTIVE' }).
	       	success(function(data) {
	       		alert("Task unmarked");
	            findAllTasks();
	            console.log("taskUri = "+taskUri);
	       });
	       $scope.selection.splice(idx, 1);
	     }
	     // is newly selected
	        // HTTP PATCH to COMPLETED state
	     else {
	       $http.patch(taskUri, { taskStatus: 'COMPLETED' }).
	       success(function(data) {
	    	   alert("Task marked completed");
	           findAllTasks();
	           console.log("taskUri = "+taskUri);
	      });
	       $scope.selection.push(taskUri);
	     }
   };

   //clear completed
   function clearCompleted() {
  	 for (var i = 0; i < $scope.tasks.length; i++) {
           if ($scope.tasks[i].taskStatus == 'COMPLETED') {
               $scope.selection.push("tasks/" + $scope.tasks[i].taskId);
           }
       }
   }
   

   // Archive Completed Tasks
   $scope.archiveTasks = function archiveTasks() {
	   clearCompleted();
       $scope.selection.forEach(function(taskUri) {
          if (taskUri != undefined) {
              console.log("taskUri = "+taskUri);
        	  $http.patch(taskUri, { taskArchived: 1});
          }
       });
       alert("Successfully Archived");
       console.log("It's risky to run this without confirming all the patches are done. when.js is great for that");
       findAllTasks();
   };
});
//Angularjs Directive for confirm dialog box
taskManagerModule.directive('ngConfirmClick', [
 function(){
         return {
             link: function (scope, element, attr) {
                 var msg = attr.ngConfirmClick || "Are you sure?";
                 var clickAction = attr.confirmedClick;
                 element.bind('click',function (event) {
                     if ( window.confirm(msg) ) {
                         scope.$eval(clickAction);
                     }
                 });
             }
         };
 }]);