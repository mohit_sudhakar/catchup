package com.mohitsudhakar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
@EnableJpaRepositories

@Import(RepositoryRestMvcConfiguration.class)
public class Application {
	
	@Bean
    public WebSecurityConfigurerAdapter webSecurityConfigurerAdapter() {
        return new MySecurityConfigurer();
    }
 
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    public static class MySecurityConfigurer extends WebSecurityConfigurerAdapter {
 
    	@Override
    	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
            builder.inMemoryAuthentication()
              .withUser("user").password("user").roles("USER")
              .and().withUser("admin").password("admin").roles("ADMIN");
        }
    	
//    	@Override
//    	protected void configure(HttpSecurity http) throws Exception {
//            http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/login.html").permitAll();
//        }
    }	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}

//The overridden method in the the static class exposes a builder instance which allows 
//you to configure your users in a very straightforward manner. We use an in-memory 
//authentication in our example, for simplicity reasons.