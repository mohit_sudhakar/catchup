package com.mohitsudhakar;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.sql.Date;
//import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name="task_list")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="task_id")
	private int id;
	
	@Column(name="task_name")
	private String taskName;
	
	@Column(name="task_description")
	private String taskDescription;
	
	@Column(name="task_priority")
	private String taskPriority;
	
	@Column(name="task_status")
	private String taskStatus;
	
	@Column(name="task_time")
	private Date taskTime;
	
	@Column(name="task_archived")
	private int taskArchived = 0;
	
	//Getter function - task_id
	public int getTaskId() {
		return id;
	}
	//Setter function - task_id
	public void setTaskId(int taskId) {
		this.id = taskId;
	}
	
	//Getter function - task_name
	 public String getTaskName() {
	  return taskName;
	 }
	//Setter function - task_name
	 public void setTaskName(String taskName) {
	  this.taskName = taskName;
	 }
	 
	//Getter function - task_description
	 public String getTaskDescription() {
	  return taskDescription;
	 }
	//Setter function - task_description
	 public void setTaskDescription(String taskDescription) {
	  this.taskDescription = taskDescription;
	 }
	 
	//Getter function - task_priority
	 public String getTaskPriority() {
	  return taskPriority;
	 }
	//Setter function - task_priority
	 public void setTaskPriority(String taskPriority) {
	  this.taskPriority = taskPriority;
	 }
	 
	//Getter function - task_status
	 public String getTaskStatus() {
	  return taskStatus;
	 }
	//Setter function - task_status
	 public void setTaskStatus(String taskStatus) {
	  this.taskStatus = taskStatus;
	 }
	 
	//Getter function - task_start_time
	public Date getTaskTime() {
		return taskTime;
	}
	//Setter function - task_start_time
	public void setTaskTime(Date taskTime) {
		this.taskTime = taskTime;
	}
	
	//Getter function - task_archived
	 public int isTaskArchived() {
	  return taskArchived;
	 }
	 //Setter function - task_archived 
	 public void setTaskArchived(int taskArchived) {
	  this.taskArchived = taskArchived;
	 }
	 
	 @Override
	 public String toString() {
	  return "Task [id=" + id + ", taskName=" + taskName
	    + ", taskDescription=" + taskDescription + ", taskPriority="
	    + taskPriority + ", taskStatus=" + taskStatus + ", taskTime=" + taskTime + "]";
	 }

	
}


